#!/bin/sh

set -e
set -u

export DJANGO_SETTINGS_MODULE=Project.prod_settings

cd /code
python manage.py migrate --noinput        # Apply database migrations
python manage.py collectstatic --noinput  # Collect static files

supervisord -c /code/Deploy/supervisor.conf
