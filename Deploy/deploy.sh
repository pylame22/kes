#!/usr/bin/env bash

# shellcheck disable=SC2164
cd /root/hahback
docker-compose build web
docker-compose up --no-deps -d web
exit
