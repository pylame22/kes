from django.apps import AppConfig


class BuildingAppConfig(AppConfig):
    name = 'apps.service'
    verbose_name = "Услуги"
