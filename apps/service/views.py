from django.views import generic
from .models import Service, ServicePage


class ServiceListView(generic.ListView):
    queryset = Service.objects.all()
    template_name = 'building/list.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['page'] = ServicePage.objects.prefetch_related('images').first()
        return context


class ServiceDetailView(generic.DetailView):
    queryset = Service.objects.all()
    template_name = 'building/detail.html'
