from django.contrib import admin
from nested_admin.nested import NestedModelAdmin, NestedTabularInline
from apps.base.mixins import BaseSortableHiddenMixin
from .models import ServicePage, ServicePageImage, Service, ServiceImage


class ServicePageImageInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = ServicePageImage


@admin.register(ServicePage)
class ServicePageAdmin(NestedModelAdmin):
    inlines = [ServicePageImageInline]

    def has_add_permission(self, request):
        return not ServicePage.objects.exists()

    def has_delete_permission(self, request, obj=None):
        return False


class ServiceImageInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = ServiceImage


@admin.register(Service)
class ServiceAdmin(NestedModelAdmin):
    inlines = [ServiceImageInline, ]
