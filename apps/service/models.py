from ckeditor.fields import RichTextField
from django.db import models


class ServicePage(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    description = RichTextField('Описание')

    class Meta:
        verbose_name = 'Страница "Услуги"'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self._meta.verbose_name


class ServicePageImage(models.Model):
    service_page = models.ForeignKey('ServicePage', on_delete=models.CASCADE, related_name='images')
    image = models.ImageField('Картинка', upload_to='service/page/')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'

    def __str__(self):
        return f'{self._meta.verbose_name} #{self.order}'


class Service(models.Model):
    name = models.CharField('Название', max_length=255)
    slug = models.SlugField('Слаг', unique=True)
    image = models.ImageField('Картинка', upload_to='service/image/')
    description = RichTextField('Описание')
    title = models.CharField('Заголовок', max_length=255)
    text = RichTextField('Текст')
    features1 = RichTextField('Преимущество 1', blank=True, null=True)
    features2 = RichTextField('Преимущество 2', blank=True, null=True)
    features3 = RichTextField('Преимущество 3', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'


class ServiceImage(models.Model):
    service = models.ForeignKey('Service', on_delete=models.CASCADE, related_name='images')
    image = models.ImageField('Картинка', upload_to='service/images/')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
        ordering = ['order']

    def __str__(self):
        return f'{self._meta.verbose_name} #{self.order}'
