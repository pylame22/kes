from django.urls import path, include
from .views import ContactPageView

app_name = 'base'

urlpatterns = [
    path('service/', include('apps.service.urls')),
    path('shop/', include('apps.shop.urls')),
    path('contacts/', ContactPageView.as_view(), name='contacts'),
    path('', include('apps.main.urls')),
]
