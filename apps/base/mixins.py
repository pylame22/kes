from nested_admin.forms import SortableHiddenMixin


class BaseSortableHiddenMixin(SortableHiddenMixin):
    extra = 0
    sortable_field_name = 'order'
