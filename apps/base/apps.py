from django.apps import AppConfig


class BaseAppConfig(AppConfig):
    name = 'apps.base'
    verbose_name = "Заявки"
