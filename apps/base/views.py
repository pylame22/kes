from django.http import HttpResponse
from django.views import generic
from .models import Application


class ContactPageView(generic.TemplateView):
    template_name = 'contacts.html'

    def post(self, *args, **kwargs):
        Application.objects.create(
            name=self.request.POST.get('name'),
            phone=self.request.POST.get('tel'),
            email=self.request.POST.get('email'),
            message=self.request.POST.get('message'),
        )
        return HttpResponse(status=200)
