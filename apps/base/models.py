from ckeditor.fields import RichTextField
from django.db import models


class Application(models.Model):
    name = models.CharField('Имя', max_length=255)
    phone = models.CharField('Телефон', max_length=12)
    email = models.EmailField('E-mail')
    message = models.TextField('Сообщение')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'
        ordering = ['-created']

    def __str__(self):
        return self.name
