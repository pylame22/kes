from django.contrib import admin
from .models import Application
from django.contrib.auth.models import Group, User

admin.site.unregister(User)
admin.site.unregister(Group)


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass
