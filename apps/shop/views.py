from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property
from django.views import generic
from .models import Category, Item, ShopPage


class CategoryListView(generic.ListView):
    queryset = Category.objects.all()
    template_name = 'shop/category_list.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['page'] = ShopPage.objects.prefetch_related('images').first()
        return context


class ItemListView(generic.ListView):
    template_name = 'shop/item_list.html'
    paginate_by = 12

    @cached_property
    def category(self):
        queryset = Category.objects.prefetch_related('types', 'manufacturers').all()
        return get_object_or_404(queryset, slug=self.kwargs['slug'])

    def get_queryset(self):
        queryset = Item.objects.select_related('category').filter(category=self.category)
        if self.request.GET.get('name'):
            queryset = queryset.filter(name__icontains=self.request.GET.get('name'))
        if self.request.GET.getlist('manufacturer'):
            queryset = queryset.filter(manufacturer_id__in=self.request.GET.getlist('manufacturer'))
        if self.request.GET.getlist('type'):
            queryset = queryset.filter(type_id__in=self.request.GET.getlist('type'))
        if self.request.GET.get('min_price'):
            queryset = queryset.filter(price__gt=self.request.GET.get('min_price'))
        if self.request.GET.get('max_price'):
            queryset = queryset.filter(price__lt=self.request.GET.get('max_price'))
        if self.request.GET.get('min_power'):
            queryset = queryset.filter(power__lt=self.request.GET.get('min_power'))
        if self.request.GET.get('max_power'):
            queryset = queryset.filter(power__lt=self.request.GET.get('max_power'))
        if self.request.GET.get('amount'):
            queryset = queryset.filter(min_amount__lt=self.request.GET.get('amount'),
                                       max_amount__gt=self.request.GET.get('amount'))
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.category
        return context


class ItemDetailView(generic.DetailView):
    queryset = Item.objects.select_related('category', 'type', 'manufacturer').all()
    template_name = 'shop/item.html'
    slug_url_kwarg = 'item_slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['similar_items'] = Item.objects.filter(category=context['object'].category) \
                                       .exclude(id=context['object'].id).order_by('?')[:3]
        return context
