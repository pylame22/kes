from django.urls import path, include
from . import views

app_name = 'shop'

urlpatterns = [
    path('', views.CategoryListView.as_view(), name='category_list'),
    path('<slug:slug>/', views.ItemListView.as_view(), name='item_list'),
    path('<slug:slug>/<slug:item_slug>/', views.ItemDetailView.as_view(), name='item_detail'),
]
