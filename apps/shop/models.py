from ckeditor.fields import RichTextField
from django.db import models


class ShopPage(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    description = RichTextField('Описание')

    class Meta:
        verbose_name = 'Страница "Магазин"'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self._meta.verbose_name


class ShopPageImage(models.Model):
    shop_page = models.ForeignKey('ShopPage', on_delete=models.CASCADE, related_name='images')
    image = models.ImageField('Картинка', upload_to='service/page/')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
        ordering = ['order']

    def __str__(self):
        return f'{self._meta.verbose_name} #{self.order}'


class Type(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Тип товара'
        verbose_name_plural = 'Типы товаров'

    def __str__(self):
        return self.name


class Manufacturer(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Производитель товара'
        verbose_name_plural = 'Производители товаров'

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField('Название', max_length=255)
    slug = models.SlugField('Слаг', unique=True)
    image = models.ImageField('Картинка', upload_to='shop/category/')
    description = RichTextField('Описание')
    title = models.CharField('Заголовок', max_length=255)
    text = RichTextField('Текст')
    types = models.ManyToManyField('Type', verbose_name='Типы товаров')
    manufacturers = models.ManyToManyField('Manufacturer', verbose_name='Производители товаров')

    footer_text = RichTextField('Текст перед футером')

    is_price = models.BooleanField('Свойство - цена', default=False)
    is_manufacturer = models.BooleanField('Свойство - производитель', default=False)
    is_type = models.BooleanField('Свойство - тип', default=False)
    is_power = models.BooleanField('Свойство - мощность (кВт)', default=False)
    is_amount = models.BooleanField('Свойство - объем парильни', default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория интернет-магазина'
        verbose_name_plural = 'Категории интернет-магазина'


class Item(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE, verbose_name='Категория')
    type = models.ForeignKey('Type', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Тип товара')
    manufacturer = models.ForeignKey('Manufacturer', on_delete=models.CASCADE, blank=True, null=True,
                                     verbose_name='Производитель')
    name = models.CharField('Название', max_length=255)
    slug = models.SlugField('Слаг', unique=True)

    price = models.PositiveIntegerField(blank=True, null=True, verbose_name='Цена')
    power = models.FloatField(blank=True, null=True, verbose_name='Мощность')
    min_amount = models.FloatField(blank=True, null=True, verbose_name='мин. объем парильни')
    max_amount = models.FloatField(blank=True, null=True, verbose_name='макс. объем парильни')
    description = RichTextField(blank=True, null=True, verbose_name='описание')
    image = models.ImageField(upload_to='shop/item/', blank=True, null=True, verbose_name='картинка')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
        ordering = ['id']
