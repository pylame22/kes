from django.contrib import admin
from nested_admin.nested import NestedTabularInline, NestedModelAdmin
from apps.base.mixins import BaseSortableHiddenMixin
from .models import Category, Item, ShopPage, ShopPageImage, Manufacturer, Type


class ShopPageImageInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = ShopPageImage


@admin.register(ShopPage)
class ShopPageAdmin(NestedModelAdmin):
    inlines = [ShopPageImageInline, ]

    def has_add_permission(self, request):
        return not ShopPage.objects.exists()

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Type)
class TypeAdmin(admin.ModelAdmin):
    pass


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    pass


@admin.register(Category)
class CategoryAdmin(NestedModelAdmin):
    pass


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    pass
