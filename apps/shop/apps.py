from django.apps import AppConfig


class ShopAppConfig(AppConfig):
    name = 'apps.shop'
    verbose_name = 'Интернет-магазин'
