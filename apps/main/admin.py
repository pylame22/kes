from django.contrib import admin
from nested_admin.nested import NestedTabularInline, NestedModelAdmin
from apps.base.mixins import BaseSortableHiddenMixin
from .models import Main, MainImage, MainFeature, MainService, MainReview, MainNews


class MainNewsInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = MainNews


class MainReviewInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = MainReview


class MainServiceInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = MainService


class MainFeatureInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = MainFeature


class MainImageInline(BaseSortableHiddenMixin, NestedTabularInline):
    model = MainImage


@admin.register(Main)
class MainAdmin(NestedModelAdmin):
    inlines = [MainImageInline, MainFeatureInline, MainServiceInline, MainReviewInline, MainNewsInline]

    def has_add_permission(self, request):
        return not Main.objects.exists()

    def has_delete_permission(self, request, obj=None):
        return False
