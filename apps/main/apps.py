from django.apps import AppConfig


class MainAppConfig(AppConfig):
    name = 'apps.main'
    verbose_name = "Главная"
