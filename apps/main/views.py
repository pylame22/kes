from django.views import generic
from .models import Main


class MainPageView(generic.DetailView):
    template_name = 'index.html'

    def get_object(self, queryset=None):
        return Main.objects.prefetch_related('images', 'features', 'services', 'reviews', 'news').first()
