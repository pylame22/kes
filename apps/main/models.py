from ckeditor.fields import RichTextField
from django.db import models


class Main(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    description = RichTextField('Описание')

    services = models.ManyToManyField('service.Service', through='MainService')

    class Meta:
        verbose_name = 'Главная'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self._meta.verbose_name


class MainImage(models.Model):
    main = models.ForeignKey('Main', on_delete=models.CASCADE, related_name='images')
    image = models.ImageField('Картинка', upload_to='main/page/')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
        ordering = ['order']

    def __str__(self):
        return f'{self._meta.verbose_name} #{self.order}'


class MainFeature(models.Model):
    main = models.ForeignKey('Main', on_delete=models.CASCADE, related_name='features')
    name = models.CharField('Название', max_length=255)
    image = models.ImageField('Каринка', upload_to='main/feature/')
    description = models.TextField('Описание')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Достижение'
        verbose_name_plural = 'Достижения'
        ordering = ['order']

    def __str__(self):
        return self.name


class MainService(models.Model):
    main = models.ForeignKey('Main', on_delete=models.CASCADE)
    service = models.ForeignKey('service.Service', on_delete=models.CASCADE)
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ['order']

    def __str__(self):
        return str(self.service)


class MainReview(models.Model):
    main = models.ForeignKey('Main', on_delete=models.CASCADE, related_name='reviews')
    name = models.CharField('Имя', max_length=255)
    review = models.TextField('Отзыв')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['order']

    def __str__(self):
        return self.review[:10]


class MainNews(models.Model):
    main = models.ForeignKey('Main', on_delete=models.CASCADE, related_name='news')
    news = models.TextField('Новость')
    icon = models.ImageField('Иконка', upload_to='main/news/')
    order = models.PositiveIntegerField('Позиция')

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ['order']

    def __str__(self):
        return self.news[:10]
