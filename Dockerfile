FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN apt-get update && apt-get install -y gdal-bin \
    supervisor \
    libffi-dev \
    libjpeg-turbo-progs \
    libtiff5-dev libjpeg62-turbo-dev libopenjp2-7-dev libjpeg-dev libpng-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev \
    python3-tk && \
    pip install --upgrade pip setuptools && pip install --no-cache-dir -r requirements.txt
COPY . /code
CMD ["./Deploy/start_in_docker.sh"]
