$(function () {
  //----------------------FEATURES----------------------------
  var featuresControlPrev = $('.feature__control--prev');
  var featuresControlNext = $('.feature__control--next');
  var featuresWrapper = $('.feature__wrapper');

  slider(featuresControlPrev, featuresControlNext, featuresWrapper, false);

  //----------------------REVIEWS----------------------------
  var reviewsControlPrev = $('.reviews__control--prev');
  var reviewsControlNext = $('.reviews__control--next');
  var reviewsWrapper = $('.reviews__wrapper');

  slider(reviewsControlPrev, reviewsControlNext, reviewsWrapper, true);

  //----------------------SLIDER-IMAGE----------------------------
  var imageSlides = $('.slider__img');
  var imageSliderDots = $('.slider__dot');
  var imageSliderControlPrev = $('.slider__control--prev');
  var imageSliderControlNext = $('.slider__control--next');
  var currentImageSlide = 0;
  var slideImageInterval = setInterval(nextImageSlide, 4000);

  function nextImageSlide() {
    goToImageSlide(currentImageSlide + 1);
  }

  imageSliderControlNext.click(function () {
    clearInterval(slideImageInterval);
    goToImageSlide(currentImageSlide + 1);
  });

  imageSliderControlPrev.click(function () {
    clearInterval(slideImageInterval);
    goToImageSlide(currentImageSlide - 1);
  });

  imageSliderDots.click(function () {
    clearInterval(slideImageInterval);
    goToImageSlide($(this).data('num'));
  });

  function goToImageSlide(newSlide) {
    $(imageSlides[currentImageSlide]).removeClass('slider__img--show');
    $(imageSliderDots[currentImageSlide]).removeClass('slider__dot--active');
    if (newSlide >= imageSlides.length ) {
      currentImageSlide = 0;
    } else if (newSlide < 0) {
      currentImageSlide = imageSlides.length - 1;
    } else {
      currentImageSlide = newSlide;
    }
    $(imageSlides[currentImageSlide]).addClass('slider__img--show');
    $(imageSliderDots[currentImageSlide]).addClass('slider__dot--active');
  }
});

function slider(controlPrev, controlNext, wrapper, adaptive) {

  var widthWrapper = wrapper.children().length * 100 - 100;
  var offset = 0;
  var nextOffset = 100;

  if (adaptive) {
    getInfo();
    $(window).resize(function () {
      getInfo();
      offset = 0;
      wrapper.css('left', '0%');
    });
  }


  var interval = setInterval(function () {
    NextItem();
  }, 5000);

  controlNext.click(function () {
    clearInterval(interval);
    NextItem();
  });

  controlPrev.click(function () {
    clearInterval(interval);
    offset -= nextOffset;
    if (offset < 0) {
      offset = widthWrapper;
    }
    wrapper.css('left', '-' + offset + '%')
  });

  function NextItem() {
    offset += nextOffset;
    if (offset > widthWrapper) {
      offset = 0;
    }
    wrapper.css('left', '-' + offset + '%');
  }

  function getInfo() {
    if ($(window).width() >= 768) {
      nextOffset = 100 / 3;
      widthWrapper = 100;
    } else {
      nextOffset = 100;
      widthWrapper = wrapper.children().length * 100 - 100;
    }
  }
}