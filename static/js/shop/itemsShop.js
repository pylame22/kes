$(function () {
  var filterHeader = $('.filter__header');
  var filterArrow = $('.filter__arrow');
  var filterFieldsets = $('.filter__fieldsets');
  var deg = 180;

  filterHeader.click(function () {
    filterFieldsets.slideToggle(300);
    filterArrow.css('transform', 'rotate(' + deg + 'deg)');
    deg += 180;
  });
});
